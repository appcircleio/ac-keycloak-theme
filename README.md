# Contribution Guide

- Clone this repository
- Create configuration folder on parent of this repository
- Disable theme and templating cache in `/opt/jboss/keycloak/standalone/configuration/standalone.xml`
  ```
  <theme>
    <staticMaxAge>-1</staticMaxAge>
    <cacheThemes>false</cacheThemes>
    <cacheTemplates>false</cacheTemplates>
    <welcomeTheme>${env.KEYCLOAK_WELCOME_THEME:keycloak}</welcomeTheme>
    <default>${env.KEYCLOAK_DEFAULT_THEME:keycloak}</default>
    <dir>${jboss.home.dir}/themes</dir>
  </theme>
  ```
- Disable gzip encoding in the browser devtools/network conditions
- Run code on parent

```
docker run -d -p 9000:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin --mount type=bind,source=${PWD}/mytheme,target=/opt/jboss/keycloak/themes/mytheme --mount type=bind,source="$(pwd)"/configuration,target=/opt/jboss/keycloak/standalone/configuration jboss/keycloak
```