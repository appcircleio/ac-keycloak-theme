<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "form">
        <div class="LoginForm_main_box">
            <div class="LoginForm_main_box_title">
                Sign up
            </div>
            <div class="LoginForm_main_box_subtitle">
                <div class="LoginForm_main_box_subtitle_defination">
                 Already have an account ?
                </div>
                <a tabindex="6" class="LoginForm_main_box_subtitle_link" href="${url.loginUrl}">Log in</a>
            </div>
            <form id="kc-form-login" onsubmit="login.disabled = true; triggerGoogleEvent('Email_Register'); return true;" action="${url.loginAction}" method="post" autocomplete="off" >
                <div class="LoginForm_main_box_inputs LoginForm_main_box_inputs_signup">
                    <div class="LoginForm_main_box_input withBorder">
                        <label>
                            <input id="email" name="email" type="email" value="${(login.email!'')}" autofocus required>
                            <span class="placeholder">Email</span>
                        </label>
                    </div>
                    <#if passwordRequired??>
                        <div class="LoginForm_main_box_input withBorder">
                            <label>
                                <input id="password" name="password" type="password" autocomplete="new-password" required>
                                <span class="placeholder">Password</span>
                            </label>
                        </div>
                        <div class="LoginForm_main_box_input">
                            <label>
                                <input id="password-confirm" name="password-confirm" type="password" autocomplete="new-password" required>
                                <span class="placeholder">Confirm your password</span>
                            </label>
                        </div>
                    </#if>
                </div>
                <div class="LoginForm_formTools">
                </div>
                <div class="LoginForm_formFooter">
                    <div class="LoginForm_formFooter_submit">
                        <button tabindex="3" type="submit" class="LoginForm_button" name="signUp">
                            <span>Sign up</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
               <div class="LoginForm_formFooter_text aggrementBox">
             <div class="LoginForm_main_box_subtitle_defination aggrementBoxText">
             By creating an account you agree to our  <a tabindex="6" target="_blank" rel="noopener noreferrer" class="LoginForm_main_box_subtitle_link LoginForm_main_box_subtitle_link_grey" href="https://appcircle.io/terms-of-service">Terms of Service</a>
             and  <a tabindex="6" target="_blank" rel="noopener noreferrer" class="LoginForm_main_box_subtitle_link LoginForm_main_box_subtitle_link_grey" href="https://appcircle.io/privacy-policy">Privacy Policy</a>
            </div>
        </div>
    </#if>
</@layout.registrationLayout>
