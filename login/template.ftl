<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false displayWide=false showAnotherWayIfPresent=true>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title>Appcircle – Automated Mobile App Release and Deployment</title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>

    <!-- Start Froged --><script>window.frogedSettings = { appId: "sbcluq" }</script><script>(function(){var a=document,b=window;if("function"!=typeof Froged){Froged=function(a,b,c){Froged.c(a,b,c)},Froged.q=[],Froged.c=function(a,b,c){Froged.q.push([a,b,c])},fg_data={hooks:{}};var c=function(){var b=a.createElement("script");b.type="text/javascript",b.async=!0,b.src="https://sdk.froged.com";var c=a.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)};"complete"===a.readyState?c():window.attachEvent?b.attachEvent("onload",c):b.addEventListener("load",c,!1)}})()</script><!-- End Froged -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40954643-13"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-40954643-13');

        function triggerGoogleEvent(eventValue){
            console.log(eventValue)
            window.gtag('event', 'login', {'method': eventValue})
        }
    </script>
</head>

<body class="BodyContainer">
    <#-- App-initiated actions should not see warning messages about the need to complete the action -->
    <#-- during login.                                                                               -->
    <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
        <div class="alert alert-${message.type}">
            <input id="message" class="hidden" value="${kcSanitize(message.summary)?no_esc}" />
            <#if message.type = 'success'><script>(function() { showSuccessToast(document.getElementById("message").value); })();</script></#if>
            <#if message.type = 'warning'><script>(function() { showWarningToast(document.getElementById("message").value); })();</script></#if>
            <#if message.type = 'error'><script>(function() { showErrorToast(document.getElementById("message").value); })();</script></#if>
            <#if message.type = 'info'><script>(function() { showInfoToast(document.getElementById("message").value); })();</script></#if>
        </div>
    </#if>
    <div class="LoginForm">
        <div class="LoginForm_sidebar">
            <img class="LoginForm_sidebar_logo" src="${url.resourcesPath}/img/appcircle_logo.svg" />
            <img class="LoginForm_sidebar_advertisement" src="${url.resourcesPath}/img/illustration.svg" />
            <div class="LoginForm_sidebar_text">Build, Deploy and Test Mobile Apps in 5 Minutes</div>
        </div>
        <div class="LoginForm_main" id="main">
          <#nested "form">
        </div>
    </div>
</body>
</html>
</#macro>
