<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "header">
        <#if messageHeader??>
        ${messageHeader}
        <#else>
        ${message.summary}
        </#if>
    <#elseif section = "form">
    <div id="kc-info-message">
        <p class="instruction">${message.summary}<#if requiredActions??><#list requiredActions>: <b><#items as reqActionItem>${msg("requiredAction.${reqActionItem}")}<#sep>, </#items></b></#list><#else></#if></p>
        <#if skipLink??>
        <#else>
            <#if pageRedirectUri?has_content>
                <div class="LoginForm_formTools">
                    <a tabindex="-1" class="LoginForm_formTools_forgot" href="${pageRedirectUri}">${kcSanitize(msg("backToApplication"))?no_esc}</a>
                </div>
            <#elseif actionUri?has_content>
                <div class="LoginForm_formTools">
                    <a tabindex="-1" class="LoginForm_formTools_forgot" href="${actionUri}">${kcSanitize(msg("proceedWithAction"))?no_esc}</a>
                </div>
            <#elseif (client.baseUrl)?has_content>
                <div class="LoginForm_formTools">
                    <a tabindex="-1" class="LoginForm_formTools_forgot" href="${client.baseUrl}">${kcSanitize(msg("backToApplication"))?no_esc}</a>
                </div>
            </#if>
        </#if>
    </div>
    </#if>
</@layout.registrationLayout>