<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>
    <#if section = "form">
        <div class="LoginForm_main_box" id="kc-form">
            <div id="IdentityProvidersSection">
                <#if realm.password && social.providers??>
                    <div id="kc-social-providers" class="LoginProviders">
                        <#list social.providers as p>
                            <a class="LoginProviders_host" href="${p.loginUrl}" onclick="triggerGoogleEvent('${p.displayName}')">
                                <div class="LoginProviders_host_icon">
                                    <img src="${url.resourcesPath}/img/${p.providerId}.svg" />
                                </div> 
                                <div class="LoginProviders_host_name"> Continue with ${p.displayName}</div>
                            </a>
                        </#list>
                    </div>
                </#if>
            </div>
            <div class="OrDivider">OR</div>  
            <div id="LoginSection">
                <form id="kc-form-login" onsubmit="login.disabled = true; continueWithLogin=true; triggerGoogleEvent('Email_Login'); return true;" action="${url.loginAction}" method="post" autocomplete="off" >
                    <div class="LoginForm_main_box_inputs">
                        <div class="LoginForm_main_box_input withBorder">
                            <label>
                                <input id="username" name="username" type="email" value="${(login.username!'')}"  type="text" autofocus  required>
                                <span class="placeholder">Email</span>
                            </label>
                        </div>
                        <div class="LoginForm_main_box_input">
                            <label>
                                <input id="password" name="password" type="password"  required autocomplete="off">
                                <span class="placeholder">Password</span>
                            </label>
                        </div>
                    </div>
                    <div class="LoginForm_formFooter">
                        <div class="LoginForm_formFooter_submit">
                            <button tabindex="3" type="submit" class="LoginForm_button LoginForm_button_md"  name="login" id="kc-login"  >
                                <span>Log in</span>
                            </button>          
                        </div>
                    </div>
                    <div class="dividerBox"><div class="divider"></div> </div>
          
                    <ul class="LoginForm_formTools">
                        <li>  <a tabindex="-1" class="LoginForm_formTools_forgot" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a> </li>
                        <li>  <a tabindex="-1" class="LoginForm_formTools_signup" href="${url.registrationUrl}">Sign up with e-mail</a> </li>
                    </ul>
                </form>
            </div>
        </div>
            <div class="LoginForm_formFooter_text aggrementBox">
             <div class="LoginForm_main_box_subtitle_defination aggrementBoxText">
             By creating an account you agree to our  <a tabindex="6" target="_blank" rel="noopener noreferrer" class="LoginForm_main_box_subtitle_link LoginForm_main_box_subtitle_link_grey" href="https://appcircle.io/terms-of-service">Terms of Service</a>
             and  <a tabindex="6" target="_blank" rel="noopener noreferrer" class="LoginForm_main_box_subtitle_link LoginForm_main_box_subtitle_link_grey" href="https://appcircle.io/privacy-policy">Privacy Policy</a>
            </div>
        </div>
    </#if>
</@layout.registrationLayout>
