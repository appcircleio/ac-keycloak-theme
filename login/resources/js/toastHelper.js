function showToast(text, backgroundColor) {
    Toastify({
        text,
        duration: 3000, 
        newWindow: true,
        close: true,
        gravity: "bottom", // `top` or `bottom`
        position: 'right', // `left`, `center` or `right`
        backgroundColor,
        stopOnFocus: true, // Prevents dismissing of toast on hover
      }).showToast();
}

function showSuccessToast(text) {
    showToast(text, "#6fba6c")
}

function showWarningToast() {
    showToast(text, "#ff8f34")
}

function showErrorToast(text) {
    showToast(text, "#da4444")
}

function showInfoToast(text) {
    showToast(text, "#5b799e")
}