<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "form">
       <div class="LoginForm_main_box">
            <div class="LoginForm_main_box_title">
                   Forgot Password?
            </div>
            <div class="LoginForm_main_box_subtitle">
                <div class="LoginForm_main_box_subtitle_defination">
                   Enter the email address associated with your account
                </div>
            </div>
            <form onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post" autocomplete="off" >
                <div class="LoginForm_main_box_inputs LoginForm_main_box_inputs_forgot">
                    <div class="LoginForm_main_box_input withBorder">
                        <label>
                            <input id="username" name="username" value="${(login.username!'')}"  type="text" autofocus  required>
                            <span class="placeholder">Email</span>
                        </label>
                    </div>
                </div>
                <div class="LoginForm_formFooter">
                    <div class="LoginForm_formFooter_submit">
                        <button tabindex="3" type="submit" class="LoginForm_button"  name="login" id="kc-login">
                            <span>Send</span>
                        </button>
                    </div>
                    <a tabindex="-1" class="LoginForm_formFooter_back" href="${url.loginUrl}">Back to login</a>
                </div>
            </form>
        </div>
    </#if>
</@layout.registrationLayout>
