<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "form">
       <div class="LoginForm_main_box">
            <div class="LoginForm_main_box_title">
                ${msg("emailVerifyTitle")}
            </div>
            <div class="LoginForm_main_box_subtitle">
                <div class="LoginForm_main_box_subtitle_defination">
                   ${msg("emailVerifyInstruction1")}
                   <br/><br/>
                   ${msg("emailVerifyInstruction2")}
                    <#--  <a tabindex="6" class="LoginForm_main_box_subtitle_hiddenLink" href="${url.loginAction}">${msg("doClickHereSmall")}</a> ${msg("emailVerifyInstruction3")}  -->
                </div>
            </div>
        </div>
    </#if>
</@layout.registrationLayout>