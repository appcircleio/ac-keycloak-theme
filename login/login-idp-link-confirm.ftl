<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "form">
           <div class="LoginForm_main_box" id="kc-form">
            <div class="LoginForm_main_box_title">
                Account Linking
            </div>
            <div class="LoginForm_main_box_subtitle">
                <div class="LoginForm_main_box_subtitle_defination">
                 ${kcSanitize(message.summary)?no_esc}
                </div>
            </div>
            <form id="kc-register-form" action="${url.loginAction}" method="post">
                <div class="LoginForm_formFooter">
                    <div class="LoginForm_formFooter_submit">
                        <#--  <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" name="submitAction" id="updateProfile" value="updateProfile">${msg("confirmLinkIdpReviewProfile")}</button>  -->
                        <button type="submit" class="LoginForm_button" name="submitAction" id="linkAccount" value="linkAccount">${msg("confirmLinkIdpContinue", idpAlias)}</button>
                    </div>
                </div>
            </form>
        </div>
    </#if>
</@layout.registrationLayout>
