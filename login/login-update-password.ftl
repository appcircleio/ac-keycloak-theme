<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "form">
       <div class="LoginForm_main_box">
            <div class="LoginForm_main_box_title">
                   Update Password
            </div>
            <div class="LoginForm_main_box_subtitle">
                <div class="LoginForm_main_box_subtitle_defination">
                   You need to change your password.
                </div>
            </div>
            <form id="kc-passwd-update-form" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post" autocomplete="off" >
                <input type="text" id="username" name="username" value="${username}" autocomplete="username" readonly="readonly" style="display:none;"/>
                <input type="password" id="password" name="password" autocomplete="current-password" style="display:none;"/>
                <div class="LoginForm_main_box_inputs LoginForm_main_box_inputs_forgot">
                    <div class="LoginForm_main_box_input withBorder">
                        <label>
                            <input type="password" id="password-new" name="password-new" autocomplete="new-password" required>
                            <span class="placeholder">New Password</span>
                        </label>
                    </div>
                    <div class="LoginForm_main_box_input">
                        <label>
                            <input type="password" id="password-confirm" name="password-confirm" autocomplete="new-password" required>
                            <span class="placeholder">Confirm Password</span>
                        </label>
                    </div>
                </div>
                <div class="LoginForm_formFooter">
                    <div class="LoginForm_formFooter_submit">
                        <button tabindex="3" type="submit" class="LoginForm_button">
                            <span>Submit</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </#if>
</@layout.registrationLayout>
